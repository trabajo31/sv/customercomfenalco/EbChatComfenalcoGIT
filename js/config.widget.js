import { 
    config 
} from './const.widget.js';
import {
    validateName, validateTypeDoc, validateDoc, validateEmail, validateCity, showPolicies, timeControl, ShowIp,
    validatePhone, validateTypeClient, validateCase, validatePolicies, MessageSystem, validatePopUp, intervalTimeControl
} from './function.widget.js';

$(function(){
    if (!window._genesys) window._genesys = {};
    if (!window._gt) window._gt = [];
    /**
    * Arreglo para construccion del widget y las funcionalidades del chat
    *
    * @date 06/12/2021
    * @author Carolina Arias
    */
    window._genesys.widgets =
    {
        main: {
            debug: false,
            theme: 'light',
            lang: 'es',
            emojis: true,
            actionsMenu: false,
            mobileMode: 'auto',
            mobileModeBreakpoint: 1000,
            i18n: config.urlDomain + 'js/lang.json',
            cookieOptions: {
                secure: true,
                path: '/',
                sameSite: 'Strict'
            }
        },
        webchat: {
            apikey: '',
            dataURL: config.urlSkill,
            emojis: true,
            actionsMenu: true,
            uploadsEnabled: true,
            enableCustomHeader: true,
            emojiList: '😀:grinning;😑:expressionles;😕:confuse;😗:kissing;😙:kissing_smiling_eyes;😛:stuck_out_tongue;😟:worried;😦:frowning;😧:anguished;😬:grimacing;😮:open_mouth;😯:hushed;😴:sleeping; 🚙:car; 👸:princess',
            chatButton: {
                enabled: true,
                template: false,
                openDelay: 1000,
                effectDuration: 300,
                hideDuringInvite: true
            },
            form: {
                wrapper: '<div></div>',
                inputs: [
                    {
                        id: 'cx_webchat_form_firstname',
                        name: 'firstname',
                        maxlength: '100',
                        placeholder: 'Nombre completo',
                        span: 'cx-error',
                        validateWhileTyping: false,
                        autocomplete: 'off',
                        wrapper: "<div class='row'><div class='col col-12'>{input}</div></div>",
                        validate: event => {
                            if (event !== 0) { return validateName(); }
                        }
                    },
                    {
                        id: 'cx_webchat_form_type',
                        name: 'tipoId',
                        type: 'select',
                        options:[
                            { value: '', text: 'Tipo de documento' },
                            { value: 'CC', text: 'Cédula de Ciudadanía' },
                            { value: 'CD', text: 'Carnet Diplomático' },
                            { value: 'CE', text: 'Cédula de Extranjería' },
                            { value: 'IE', text: 'ID Extranjero' },
                            { value: 'NIT', text: 'NIT' },
                            { value: 'PA', text: 'Pasaporte' },
                            { value: 'RC', text: 'Registro Civil' },
                            { value: 'TI', text: 'Tarjeta de Identidad' }
                        ],
                        span: 'cx-error',
                        validateWhileTyping: !0,
                        wrapper: "<div class='row'><div class='col col-12'>{input}</div></div>",
                        validate: event => {
                            if (event !== 0) { return validateTypeDoc(); }
                        }
                    },
                    {
                        id: 'cx_webchat_form_doc2',
                        name: 'Identify2',
                        maxlength: '12',
                        placeholder: 'Documento',
                        validateWhileTyping: false,
                        autocomplete: 'off',
                        wrapper: "<div class='row'><div class='col col-12'>{input}</div></div>",
                        validate: event => {
                            if (event !== 0) { return validateDoc(); }
                        }
                    },
                    {
                        id: 'cx_webchat_form_doc',
                        name: 'Identify',
                        maxlength: '12',
                        type: 'hidden',
                        value: ''
                    },
                    {
                        id: 'cx_webchat_form_email',
                        name: 'email',
                        maxlength: '100',
                        placeholder: 'Correo Electronico',
                        type: 'Email',
                        span: 'cx-error',
                        validateWhileTyping: false,
                        autocomplete: 'off',
                        wrapper: "<div class='row mb-20'><div class='col col-12'>{input}</div></div>",
                        validate: event => {
                            if (event !== 0) { return validateEmail(); }
                        }
                    },
                    {
                        id: 'cx_webchat_form_ciudad',
                        name: 'ciudad',
                        maxlength: '100',
                        placeholder: 'Ciudad',
                        span: 'cx-error',
                        autocomplete: 'off',
                        validateWhileTyping: !0,
                        wrapper: "<div class='row'><div class='col col-12'>{input}</div></div>",
                        validate: event => {
                            if (event !== 0) { return validateCity(); }
                        }
                    },
                    {
                        id: 'cx_webchat_form_phone',
                        name: 'PhoneNumber',
                        maxlength: '10',
                        placeholder: 'Celular',
                        validateWhileTyping: false,
                        autocomplete: 'off',
                        wrapper: "<div class='row'><div class='col col-12'>{input}</div></div>",
                        validate: event => {
                            if (event !== 0) { return validatePhone(); }
                        }
                    },
                    {
                        id: 'cx_webchat_form_yo',
                        name: 'yoSoy',
                        type: 'select',
                        options:[
                            { value: '', text: 'Yo soy' },
                            { value: 'Empresa', text: 'Empresa' },
                            { value: 'Persona', text: 'Persona' }
                        ],
                        span: 'cx-error',
                        validateWhileTyping: !0,
                        wrapper: "<div class='row'><div class='col col-12'>{input}</div></div>",
                        validate: event => {
                            if (event !== 0) { return validateTypeClient(); }
                        }
                    },
                    {
                        id: 'cx_webchat_form_tema',
                        name: 'Tema',
                        type: 'select',
                        options:[
                            { value: '', text: 'Tema' },
                            { value: 'Empresa', text: 'Para subsidios, afiliaciones, créditos, cursos, programas, entre otros.' },
                            { value: 'Persona', text: 'Para hoteles, parques, viajes nacionales, viajes internacionales, reservas' }
                        ],
                        span: 'cx-error',
                        validateWhileTyping: !0,
                        wrapper: "<div class='row'><div class='col col-12'>{input}</div></div>",
                        validate: event => {
                            if (event !== 0) { return validateCase(); }
                        }
                    },
                    {
                        id: 'cx_webchat_form_iporigen',
                        name: 'IpOrigen',
                        maxlength: '100',
                        type: 'hidden',
                        value: ''
                    },
                    {
                        id: 'cx_webchat_form_lastname',
                        name: 'lastname',
                        maxlength: '100',
                        placeholder: 'lastname',
                        type: 'hidden',
                        value: '  '
                    },
                    {
                        id: 'cx_webchat_form_subject',
                        name: 'subject',
                        maxlength: '100',
                        placeholder: 'subject',
                        type: 'hidden',
                        value: config.typeChat
                    },
                    {
                        id: 'cx_webchat_form_transcripcion',
                        name: 'Send_Chat_Transcript',
                        maxlength: '100',
                        placeholder: 'transcripcion',
                        type: 'hidden',
                        value: 'true'
                    },
                    {
                        id: 'cx_webchat_form_politicas',
                        name: 'sitio',
                        type: 'checkbox',
                        span: 'cx-error',
                        value: 'true',
                        label: "Aceptar <a class='linkPolicies' title='Términos y Condciones'>Términos y Condiciones</a>",
                        wrapper: "<div class='row mb-20'><div class='col col-12'>{input} {label}</div></div>",
                        validate: (event, form, input, label, $) => {
                            if (event !== 0) { return validatePolicies(event, input); }
                        }
                    }
                ]
            },
            markdown: false
        }
    };

    if(!window._genesys.widgets.extensions){
        window._genesys.widgets.extensions = {};
    }

    /**
    * Funcion para la creacion de una extencion del widget para poder ejecutar comandos y suscribirse a los eventos
    *
    * @date 06/12/2021
    * @author Carolina Arias
    */
    window._genesys.widgets.extensions["MyPlugin"] = function($, CXBus, Common){

        var oMyPlugin = window._genesys.widgets.bus.registerPlugin('MyPlugin');

        oMyPlugin.subscribe('WebChat.opened', function(e){
            timeControl().then(scheduleValid => {
                if(!scheduleValid.booleanValid){
                    $('.cx-webchat .cx-body').html(config.HTMLtimeControl);
                    $('.cx-webchat').addClass('controlHorario');
                } else{ 
                    $(".cx-menu-cell").after(`<div class='row mt-20'>
                        <div class='col col-12'>
                            <input class='cx-input cx-form-control' id='cx_webchat_form_encuesta' disabled name='cx_webchat_form_encuesta' type='checkbox' validatewhiletyping='false' tabindex='-1' aria-invalid='true' checked>
                            <label class='cx-control-label i18n' for='cx_webchat_form_encuesta'>Realizar encuesta</label>
                        </div>
                    </div>`);
                    ShowIp();
                    $('#cx_webchat_form_firstname').focus();
                    intervalTimeControl(scheduleValid.intMiliseg);
                    $('.cx-input-container .cx-message-input').attr('disabled');
                    /**
                    * Comando para obtener los datos de session del chat
                    * 
                    * @date 06/12/2021
                    * @author Maycol David Sánchez Mora
                    */
                    oMyPlugin.command('WebChatService.getSessionData').done(ex => {
                        if(ex.SessionID != "") { 
                            oMyPlugin.command('WebChatService.getAgents').done(function(ega) {
                                $.each(ega.agents, function(_index, value) {
                                    if (value.connected) {
                                        config.boooleanAgentConected = true;
                                        $('#cx_webchat_form_encuesta').removeAttr('disabled');
                                    }
                                });
                            });
                        }
                    });
                }
            });
        });

        /**
        * Suscripción al evento de que restaura el chat
        *
        * @date 07/12/2021
        * @author Luis Manuel Hernandez Jimenez
        */
        oMyPlugin.subscribe('WebChatService.restore', function(e){
            setTimeout(() => {
                if(config.boooleanAgentConected) {
                    $('#cx_input')[0].readOnly = false;
                } else {
                    $('#cx_input')[0].readOnly = true;
                }
            }, 500);
        });

        /**
        * Captura el evento de apertura del popup de las piliticas de tratamiento
        *
        * @date 06/12/2021
        * @author Carolina Arias
        */
        oMyPlugin.subscribe('Toaster.opened', function(e){
            $('body').append('<div id="toast-backdrop"></div>');
            $('#toast-backdrop').css('visibility', 'visible');
            oMyPlugin.command('WebChat.minimize');
        });

        /**
        * Captura el evento de cerrado del popup de las piliticas de tratamiento
        *
        * @date 06/12/2021
        * @author Carolina Arias
        */
        oMyPlugin.subscribe('Toaster.closed', function(e){
            $('#toast-backdrop').css('visibility', 'hidden');
            $('#toast-backdrop').remove();
            oMyPlugin.command('WebChat.minimize');
        });

        /**
        * Captura nuevamente SessionID cuando se recarga la página
        *
        * @date 06/12/2021
        * @author Carolina Arias
        */
        oMyPlugin.subscribe('WebChatService.messageReceived', function(e){
            if(config.SessionID == ""){ config.SessionID = e.data.sessionData.SessionID; }
            e.data.messages.forEach(element => {
                custom_menssage(element);
            });
        });

                /**
        * Capturar SessionID y nombre del cliente
        *
        * @date 06/12/2021
        * @author Luis Manuel Hernandez Jimenez
        */
        oMyPlugin.subscribe('WebChatService.started', function(e){
            config.SessionID = e.data.sessionID;
            $('.cx-menu-cell').hide();
            $('#cx_input')[0].readOnly = true;
        });

        /**
        * evento para guardar los datos del formulario en cache
        * 
        * @date 08/03/2021
        * @author Maycol Sanchez
        */
        oMyPlugin.subscribe('WebChat.submitted', function(e){ 
            Cookies.set('formDataChat', JSON.stringify(e.data.form), { sameSite: 'strict', secure: true, expires: 1 });
        });

        /**
        * Identifica que el agente esté conectado luego de que se recargue la página
        * 
        * @date 06/12/2021
        * @author Carolina Arias
        */
         oMyPlugin.subscribe('WebChatService.restored', function(ex) {
             setTimeout(() => {
                $('#cx_input')[0].readOnly = true;
            }, 500);
            oMyPlugin.command('WebChatService.getAgents').done(function(e) {
                $.each(e.agents, function(_index, value) {
                    if (value.connected) {
                        config.boooleanAgentConected = true;
                    }
                });
            });
        });

        /**
        * Mostrar encuesta cuando finaliza el chat
        * 
        * @date 06/12/2021
        * @author Carolina Arias
        */
        oMyPlugin.subscribe('WebChat.completed', function(e){
            if($('#cx_webchat_form_encuesta').is(':checked')){
                let formDataChat = JSON.parse(Cookies.get('formDataChat'));
                Cookies.remove('formDataChat');
                let chat_tipoDoc = formDataChat.tipoId;
                let chat_documento = formDataChat.Identify; 
                let chat_email = formDataChat.email; 
                let chat_nombre = formDataChat.firstname;

                let strMessage = config.urlLimeSurvey + "&id_interaccion=" + config.SessionID + "&nombre_cliente=" + chat_nombre + "&asesor=" + config.stringNameAgent + "&identificacion_cliente=" + chat_tipoDoc + "-" + chat_documento + "&correo=" + chat_email;
                window.open(strMessage);
            }
            setInterval(()=>{
                oMyPlugin.command('WebChat.close');
            }, 10000);
        });

        /**
        * Suscripcion al evento de chat finalizado, este evento se ejecuto cuando el chat finaliza independientemente de si hubo o no conexion con el agente
        * si el chat presenta encuesta en esta opcion se realizaria el llamado de esta
        *
        * @date 06/12/2021
        * @author Carolina Arias
        */
        oMyPlugin.subscribe('WebChatService.ended', function(e){
            MessageSystem(config.stringNameUser + 'ha abandonado el chat');
            config.boooleanAgentConected = false;
        });

        /**
        * Comando de chat pre registro de informacion, en este comando se realiza la validacion que el campo de mensajes del chat no envie direcciones web
        * 
        * @date 06/12/2021
        * @author Carolina Arias
        */
        oMyPlugin.command('WebChatService.registerPreProcessor', { preprocessor: function(message){
            const text = ''+message.text;
            let booleanIsUrlAcept = false;
            const urlsAccept = [
                'https://www.servicioscomfenalco.com/creditoweb/consultas/ingresousuarios.php',
                'https://www.comfenalcoantioquia.com.co/personas/servicios/creditos',
                'https://www.servicioscomfenalco.com/creditoweb/consultas/ingresousuarios.php',
                'https://www.comfenalcoantioquia.com.co/personas/noticias/pasos-subsidio-emergencia',
                'https://subsidioemergencia.asopagos.com/ASOPAGOS/jsp/zenithFront/index.html#/preRadicacionEnLinea?param=cGFyYW1ldHJvX2VzcGVyYWRvPTA0',
                'https://servicios.comfenalcoantioquia.com/comfenalcoantioquia-estadocesante/',
                'https://www.comfenalcoantioquia.com.co/empresas/afiliar-empleados',
                'http://biblioteca.comfenalcoantioquia.com/cgi-olib/'
            ];
            const url = text.match(/[a-z0-9\-\.]+[^www\.](\.[a-z]{2,4})[a-z0-9\/\-\_\.]*/i);

            $.each(urlsAccept, function (_indexInArray, valueOfElement) { 
                if(text.includes(valueOfElement)) { booleanIsUrlAcept = true; }
            });

            const regexDomail = /https:\/\/www.comfenalcoantioquia.com.co|https:\/\/www.servicioscomfenalco.com|https:\/\/viajes.comfenalcoantioquia.com.co|https:\/\/www.matriculascomfenalcoantioquia.com.co/;

            if(text.match(regexDomail)){
                booleanIsUrlAcept = true;
            }



            // if (url !== null && !booleanIsUrlAcept) {
            //     const text_replace = text.replace(url[0], '').replace('https://', '').replace('http://', '');
            //     message.text = text_replace;
            //     $('#cx_input').val(text_replace);
            //     window._genesys.widgets.common.showAlert($('.cx-widget.cx-webchat'), {text: 'No se permite enviar direciones url al chat', buttonText: 'Ok'});
            // } else if(!booleanIsUrlAcept) {
            //     const text_replace = text.replace('https://', '').replace('http://', '');
            //     message.text = text_replace;
            // } else {
            //     //Something
            // }
            return message;
        }});

        /**
        * Suscripcion al evento de chat cuando un cliente se conecta, en este evento se agregan los mensajes de espera mientras un agente logra conectarse
        * 
        * @date 07/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.subscribe('WebChatService.clientConnected', function(e){
            config.stringNameUser = e.data.message.from.name;

            MessageSystem(config.stringNameUser + 'ha iniciado sesión');

            setTimeout(()=>{
                if(!config.booleanWait){
                    MessageSystem('Sr(a), en un momento uno de nuestros asesores estará con usted. Por favor continúe en línea.');
                }
            },6000);
        });

        /**
        * Suscripcion al evento de chat cuando un agente se conecta, en este evento se agregan los mensajes de espera mientras un agente logra conectarse
        * 
        * @date 07/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.subscribe('WebChatService.agentConnected', function(e){
            $('.cx-input-container .cx-message-input').removeAttr('disabled');
            $('#cx_webchat_form_encuesta').removeAttr('disabled');
            config.boooleanAgentConected  = true;
            $('.cx-menu-cell').show();
            setTimeout(() => {
                $('#cx_input')[0].readOnly = false;
            }, 500);
            playSound();
            config.booleanWait= true;
            config.stringNameAgent = e.data.agents.name;
        });

    };

    $.ajaxSetup({
        beforeSend: function (xhr){
           xhr.setRequestHeader('Cache-Control','must-revalidate, no-cache, no-store, private');
           xhr.setRequestHeader('Pragma','no-cache');
        }
    });

    $('body').on('keyup', '#cx_webchat_form_phone', function(){
        this.value = this.value.replace(/[^0-9\.]/g,'');
    }).on('keyup', '#cx_webchat_form_doc2', function(){
        this.value = this.value.replace(/[^0-9\.]/g,'');
        const Identify = $('#cx_webchat_form_type').val() + '-' + $('#cx_webchat_form_doc2').val();
        $('#cx_webchat_form_doc').val(Identify);
    }).on('change', '#cx_webchat_form_type', function(){
        const Identify = $('#cx_webchat_form_type').val() + '-' + $('#cx_webchat_form_doc2').val();
        $('#cx_webchat_form_doc').val(Identify);
    }).on('keypress', '#cx_input', function(){
        if(!config.boooleanAgentConected){ return false; }
    }).on('click', '.linkPolicies', function(){
        showPolicies();
    });

    $(window).bind("focus",function(_event){
        config.windowFocus = true;
        if(config.popUp != null) {
            config.popUp.close();
        }
    }).bind("blur", function(_event){
        config.windowFocus = false;
    });

    (function (o) {
        var f = function () {
            var d = o.location;
            o.aTags = o.aTags || [];
            o.aTags.forEach(oTag => {
                var fs = d.getElementsByTagName(oTag.type)[0], e;
                if (!d.getElementById(oTag.id)) {
                    e = d.createElement(oTag.type); 
                    e.id = oTag.id;
                }
                if (oTag.type == "script") {
                    e.src = oTag.path;
                }
                else {
                    e.type = 'text/css'; e.rel = 'stylesheet'; e.href = oTag.path;
                }
                if(oTag.integrity){
                    e.integrity = oTag.integrity;
                    e.setAttribute('crossorigin', 'anonymous');     
                }
                if (fs) {
                    fs.parentNode.insertBefore(e, fs);
                } else {
                    d.head.appendChild(e);
                }    
            });
        }, ol = window.onload;
        if (o.onload) {
            typeof window.onload != "function" ? window.onload = f : window.onload = function () { ol(); f(); };
        }
        else f();
    })
    ({
        location: document,
        onload: false,
        aTags:
            [
                { 
                    type: "link", 
                    id: "widget-chats-styles", 
                    path: config.urlDomain + "css/webchat.css",
                    integrity: false
                },
                { 
                    type: "link", 
                    id: "widget-menu-styles", 
                    path: config.urlDomain + "css/widgets.menu.css",
                    integrity: false
                },
                { 
                    type: "link", 
                    id: "widget-icons-styles", 
                    path: config.urlDomain + "css/icons.css",
                    integrity: false
                },
                { 
                    type: "link", 
                    id: "widget-rows-styles", 
                    path: config.urlDomain + "css/rows.min.css",
                    integrity: false
                },
                { 
                    type: "link", 
                    id: "widget-toast-styles", 
                    path: config.urlDomain + "css/toast.min.css",
                    integrity: false
                },
                { 
                    type: "script", 
                    id: "cookies-script", 
                    path: config.urlDomain + "js/jquery.cookie.min.js",
                    integrity: false
                },
                { 
                    type: "script", 
                    id: "genesys-cx-widget-script", 
                    path: config.urlDomain + "js/widgets.min.js",
                    integrity: false
                },
                { 
                    type: "link", 
                    id: "genesys-cx-widget-styles", 
                    path: config.urlDomain + "css/widgets.min.css",
                    integrity: false
                }
            ]
    });
});

/**
* Suscripcion al evento de chat cuando un cliente se conecta,
en este evento se agregan los mensajes de config.booleanWait mientras un agente logra conectarse
*
* @date 07/12/2021
* @author Carolina Arias
*/
function custom_menssage(element) {
    if (element.type === 'Message' && element.from.type === 'Agent' ){
        playSound();
        if(!config.windowFocus){ validatePopUp(); }
    } else if(element.from.type === 'Client' && element.type === 'Message'){
        config.stringNameUser = element.from.name;
    } else { /** Something */ }
}


/**
* Funcion para ejecutar el sonido del beep al momento que llega un mensaje
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return NULL
*/
function playSound() {
    var thisSound = new Audio(config.urlDomain + "sound/beep.mp3");
    thisSound.play();
}
