import { 
    config 
} from './const.widget.js';

/**
* Funcion para la creacion de mensajes de sistemas
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param string stringMsg
*/
export function MessageSystem(stringMsg){
    window._genesys.widgets.bus.command('WebChat.injectMessage', {
        type: 'text',
        text: stringMsg,
        custom: false,
    });
}

//Funcion para ontener fecha server
/**
* Funcion para obtener fecha desde la api horario
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return date dateTime
*/
function timeServer(){
    let dateTime;
    $.ajax({
        type: 'POST',
        async: false,
        url: config.urlTimeServer,
        data: null,
        dataType: 'json',
        success: data => {
            dateTime = data;
        },
        error: () => {
            const fecha = new Date();
            dateTime = {
                hour: fecha.getHours(),
                minutes: fecha.getMinutes(),
                seconds: fecha.getSeconds()
            };
        }
    });
    return dateTime;
}

/**
* Funcion que define el intervalo el control de horario llamado a la funcion intervalFuntionTimeControl que ejecuta la consulta del control de horario
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return NULL
*/
export function intervalTimeControl(intMilseg){
    config.intervalTimeC = setInterval(() => {
        intervalFuntionTimeControl();
    }, intMilseg);
}

/**
* Funcion que ejecuta el control de horario llamado desde la funcion de intervalo intervalTimeControl
* 
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return promise 
*/
export function intervalFuntionTimeControl(){
    return new Promise(resolve => {
        timeControl().then(arraytimeControl => {
            clearInterval(config.intervalTimeC);
            if(!arraytimeControl.booleanValid){
                window._genesys.widgets.bus.command('WebChatService.getSessionData').done((ex) => {
                    if(ex.sessionID == "") {
                        $('.cx-webchat .cx-body').html(config.HTMLtimeControl);
                        $('.cx-webchat').addClass('timeControl');
                    } else {
                        MessageSystem("no se pudo asignar un asesor, se finalizara la interacion debido a que estamos fuera de horario");
                        setTimeout(() => {
                            window._genesys.widgets.bus.command('WebChatService.endChat');
                        }, 8000);
                    }
                });
            } else { 
                intervalTimeControl(arraytimeControl.intMiliseg);
            }
            resolve();
        });
    });
}

/**
* Funcion para Validar el Control de Horario.
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return promise arraytimeControl
*/
export function timeControl() {
    return new Promise((resolve, reject) => {
        var arraytimeControl = {};
        var dateTimeServer = timeServer();
        var dateNow = new Date();
        var dateWeek = parseInt(dateNow.getUTCDay()) + 1;
        var dateHour = parseInt(dateTimeServer.hour);
        var dateMinutes = parseInt(dateTimeServer.minutes);
        var dateSeconds = parseInt(dateTimeServer.seconds);
        var dateHourActualy = prefix(dateHour)+":"+prefix(dateMinutes)+":"+prefix(dateSeconds);

        if (dateWeek == 1 ) {
            arraytimeControl =  {
                booleanValid: false
            };
        } else if (dateWeek == 7) {
            if (dateHourActualy < config.timeControlWeekend[0] || dateHourActualy >= config.timeControlWeekend[1]) {
                arraytimeControl = { 
                    booleanValid: false,
                };
            }else{
                arraytimeControl = { 
                    booleanValid: true,
                    intMiliseg: calculateDifference(dateHourActualy, config.timeControlWeekend[2])
                };
            }
        } else {
            if (dateHourActualy < config.timeControlWeek[0] || dateHourActualy >= config.timeControlWeek[1]) {
                arraytimeControl = { 
                    booleanValid: false
                }
            } else {
                arraytimeControl = { 
                    booleanValid: true,
                    intMiliseg: calculateDifference(dateHourActualy, config.timeControlWeek[2])
                }
            }
        }
        resolve(arraytimeControl);
    });
}

/**
* Funcion para obtener la ip del usuario
* 
* @date 07/12/2021
* @author Carolina Arias
* @param NULL
* @return NULL
*/ 
export function ShowIp() {
    setTimeout(() => {
        $(function getip() {
            $.getJSON("https://api.ipify.org?format=jsonp&callback=?",
                function(json) {
                    $("#cx_webchat_form_iporigen").val(json.ip);
                }
            );
        });
    }, 500);
}

/**
* Funcion para validar el nombre del usuario
* 
* @date 11/12/2021
* @author Carolina Arias
* @param NULL
* @return boolean: booleanValidName
*/
export function validateName() {
    let booleanValidName = false;
    var x = $("#cx_webchat_form_firstname").val();
    if (x == "") {
        alertName(booleanValidName, true);
    } else {
        if (validateTypeText(x)) {
            if(validateLenght("cx_webchat_form_firstname", "alertnamedv", 6, 30)){
                if($('#alertnamedv').length){
                    $('#alertnamedv').remove();
                } 
                booleanValidName = true;
            }             
        } else {
            alertName(booleanValidName, false);
        }
    }
    return booleanValidName;
}

/**
* Funcion para validar el documento del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return boolean: booleanValidTypeIdentify
*/
export function validateTypeDoc() {
    let booleanValidTypeIdentify = false;
    var x = $("#cx_webchat_form_type_doc").val();
    if (x == "") {
        alertTypeDoc(booleanValidTypeIdentify);
    } else {
        if($('#alerttypedocdv').length){
            $('#alerttypedocdv').remove();
        }
        booleanValidTypeIdentify = true;
    }
    return booleanValidTypeIdentify;
}

/**
* Funcion para validar el documento del usuario
*
* @date 11/12/2021
* @author Carolina Arias
* @param NULL
* @return boolean: booleanValidIdentify
*/
export function validateDoc() {
    let booleanValidIdentify = false;
    var x = $("#cx_webchat_form_doc2").val();
    if (x == "") {
        alertIndentify(booleanValidIdentify, true);
    } else {
        if (validateTypeNumber(x)) {
            if(validateLenght("cx_webchat_form_doc2", "alertidentifydv", 7, 16)){
                if($('#alertidentifydv').length){
                    $('#alertidentifydv').remove();
                }
                booleanValidIdentify = true;
            } 
        } else {
            alertIndentify(booleanValidIdentify, false);
        }
    }
    return booleanValidIdentify;
}

/**
* Funcion para validar el numero de telefono del usuario
* 
* @date 11/12/2021
* @author Carolina Arias
* @param NULL
* @return boolean: booleanValidPhone
*/ 
export function validatePhone() {
    let booleanValidPhone = false;
    var x = $("#cx_webchat_form_phone").val();
    if (x == "") {
        alertPhone(booleanValidPhone, true);
    } else {
        if (validateTypeNumber(x)) {
            if(validateLenght("cx_webchat_form_phone", "alertphonedv", 3, 10)){
                if($('#alertphonedv').length){
                    $('#alertphonedv').remove();
                } 
                booleanValidPhone = true;
            }
        } else {
            alertPhone(booleanValidPhone, false);
        }
    }
    return booleanValidPhone;
}

/**
* Funcion para validar el email del usuario
*
* @date 11/12/2021
* @author Carolina Arias
* @param NULL
* @return boolean: booleanValidEmail
*/
export function validateEmail() {
    let booleanValidEmail = false;
    var x = $("#cx_webchat_form_email").val();
    if (x == "") {
        alertEmail(booleanValidEmail, true);
    } else {
        if(!validateTypeEmail(x)){
            alertEmail(booleanValidEmail, false);
        }else{
            if(validateLenght("cx_webchat_form_email", "alertemaildv", 7, 60)){
                if($('#alertemaildv').length){
                    $('#alertemaildv').remove();
                } 
                booleanValidEmail = true;
            }
        }
    }
    return booleanValidEmail;
}

/**
* Funcion para validar ciudad
* 
* @date 11/12/2021
* @author Carolina Arias
* @param NULL
* @return boolean: booleanValidCity
*/ 
export function validateCity() {
    let booleanValidCity = false;
    var x = $("#cx_webchat_form_ciudad").val();
    if (x == "") {
        alertCity(booleanValidCity, true);
    } else {
        if (validateTypeText(x)) {
            if(validateLenght("cx_webchat_form_ciudad", "alertcitydv", 1, 30)){
                if($('#alertcitydv').length){
                    $('#alertcitydv').remove();
                } 
                booleanValidCity = true;
            }             
        } else {
            alertCity(booleanValidCity, false);
        }
    }
    return booleanValidCity;
}

/**
* Funcion para validar el campo yoSoy
* 
* @date 11/12/2021
* @author Carolina Arias
* @param NULL
* @return boolean: booleanValidTypeClient
*/ 
export function validateTypeClient(){
    let booleanValidTypeClient = false;
    var x = $("#cx_webchat_form_yo").val();
    if (x == "") {
        alertTypeClient(booleanValidTypeClient);
    } else {
        if($('#alertyosoy').length){
            $('#alertyosoy').remove();
        }
        booleanValidTypeClient = true;
    }
    return booleanValidTypeClient;
}

/**
* Funcion para validar el tema
*
* @date 11/12/2021
* @author Carolina Arias
* @param NULL
* @return boolean: booleanValidCase
*/
export function validateCase() {
    let booleanValidCase = false;
    var x = $("#cx_webchat_form_tema").val();
    if (x == "") {
        alertCase(booleanValidCase, true);
    } else {
        if (validateTypeText(x)) {
            if(validateLenght("cx_webchat_form_tema", "alerttemadv", 1, 30)){
                if($('#alerttemadv').length){
                    $('#alerttemadv').remove();
                }
                booleanValidCase = true;
            }
        } else {
            alertCase(booleanValidCase, false);
        }
    }
    return booleanValidCase;
}

/**
* Funcion para validar el numero el aceptamiento de politicas del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return boolean: booleanValidEmail
*/
export function validatePolicies(event, input) {
    let booleanValidPoliticas = true;
    if (event.type != 'blur' || $.isEmptyObject(event)) {
        let checkedPolicies = $(input).is(':checked');
        if(!checkedPolicies){
            _genesys.widgets.common.showAlert($('.cx-widget.cx-webchat'), {text: 'Para continuar es necesario leer, entender y autorizar la Política de Tratamiento de Datos', buttonText: 'Ok'});
            booleanValidPoliticas = false;
        } 
        
        return booleanValidPoliticas;
    }
}

/**
* Funcion para validar el tamaño de los campos
* 
* @date 11/12/2021
* @author Carolina Arias
* @param string: input
* @param string: inputAlert
* @param int: intMin
* @param int: intMax
* @return boolean: Valid
*/ 
function validateLenght(input, inputAlert, min, max) {
    var x = $("#"+input).val();
    var valid = true;
    if(x.length < min || x.length > max){
        alertLenght(input, inputAlert, min, max);
        valid = false;
    }
    return valid;
}

/**
* Funcion para mostrar el mensaje de validacion del campo nombre del usuario
* 
* @date 11/12/2021
* @author Carolina Arias
* @param boolean: booleanValidName
* @param boolean: booleanEmpty
* @return NULL
*/ 
function alertName(booleanValidName, booleanEmpty){
    if(!booleanValidName){
        if($('#alertnamedv').length){
            $('#alertnamedv').remove();
        }
        let stringMessage = booleanEmpty ? "Ingresa tu nombre" : "Has ingresado un nombre invalido, recuerda que este campo debe contener solo texto.";
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode(stringMessage);
        spanName.style.cssText = 'color:red';
        spanName.id = 'alertnamedv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_firstname').parent('.col').append(spanName);                                  
    }
}

/**
* Funcion para mostrar el mensaje de validacion del campo tipo de documento del usuario
* 
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param boolean: booleanValidTypeDoc
* @return NULL
*/ 
function alertTypeDoc(booleanValidTypeDoc) {
    if (!booleanValidTypeDoc) {
        if($('#alerttypedocdv').length){
            $('#alerttypedocdv').remove();
        } 
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode("Selecione un tipo de documento");
        spanName.style.cssText = 'color:red';
        spanName.id = 'alerttypedocdv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_type_doc').parent('.col').append(spanName);
    }
}

/**
* Funcion para mostrar el mensaje de validacion del campo documento del usuario
* 
* @date 11/12/2021
* @author Carolina Arias
* @param boolean: booleanValidIdentify
* @param boolean: booleanEmpty
* @return NULL
*/ 
function alertIndentify(booleanValidIdentify, booleanEmpty) {
    if (!booleanValidIdentify) {
        if($('#alertidentifydv').length){
            $('#alertidentifydv').remove();
        } 
        let stringMessage = booleanEmpty ? "Ingresa tu identificación" : "Este campo debe ser numérico.";   
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode(stringMessage);
        spanName.style.cssText = 'color:red';
        spanName.id = 'alertidentifydv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_doc2').parent('.col').append(spanName);
    }
}

/**
* Funcion para mostrar el mensaje de validacion del campo telefono del usuario
* 
* @date 11/12/2021
* @author Carolina Arias
* @param boolean: booleanValidPhone
* @param boolean: booleanEmpty
* @return NULL
*/ 
function alertPhone(booleanValidPhone, booleanEmpty){
    if(!booleanValidPhone){  
        if($('#alertphonedv').length){
            $('#alertphonedv').remove();
        }   
        let stringMessage = booleanEmpty ? "Ingresa tu numero de telefono" : "Has ingresado un numero invalido, recuerda que este campo debe ser numérico.";    
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode(stringMessage);
        spanName.style.cssText = 'color:red';
        spanName.id = 'alertphonedv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_phone').parent('.col').append(spanName);                       
    }
}

/**
* Funcion para mostrar el mensaje de validacion del campo email del usuario
* 
* @date 11/12/2021
* @author Carolina Arias
* @param boolean: booleanValidEmail
* @param boolean: booleanEmpty
* @return NULL
*/ 
function alertEmail(booleanValidEmail, booleanEmpty){
    if(!booleanValidEmail){
        if($('#alertemaildv').length){
            $('#alertemaildv').remove();
        } 
        let stringMessage = booleanEmpty ? "Ingresa tu correo electrónico, ej:usuario@ejemplo.com" : "Has ingresado un correo invalido, revisa la información.";   
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode(stringMessage);
        spanName.style.cssText = 'color:red';
        spanName.id = 'alertemaildv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_email').parent('.col').append(spanName);             
    }
}

/**
* Funcion para mostrar el mensaje de validacion de los tamaños de los campos
* 
* @date 11/12/2021
* @author Carolina Arias
* @param string: input
* @param string: inputAlert
* @param int: intMin
* @param int: intMax
* @return NULL
*/ 
function alertLenght(input, inputAlert, intMin, intMax) {
    if($('#'+inputAlert).length){
        $('#'+inputAlert).remove();
    }    
    var spanName = document.createElement("div");
    var contentSpan = document.createTextNode("El campo debe contener entre "+intMin+" y "+intMax+" caracteres");
    spanName.style.cssText = 'color:red';
    spanName.id = inputAlert;
    spanName.classList.add("wc-error");
    spanName.appendChild(contentSpan);

    $('#'+input).parent('.col').append(spanName);             
}

/**
* Funcion para mostrar el mensaje de validación de la ciudad
* 
* @date 11/12/2021
* @author Carolina Arias
* @param String: booleanValidCity
* @param boolean: booleanEmpty
* @return boolean: 
*/ 
function alertCity(booleanValidCity, booleanEmpty){
    if(!booleanValidCity){
        if($('#alertcitydv').length){
            $('#alertcitydv').remove();
        }
        let stringMessage = booleanEmpty ? "Ingresa una ciudad" : "Has ingresado una ciudad invalida, recuerda que este campo debe ser solo texto.";   
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode(stringMessage);
        spanName.style.cssText = 'color:red';
        spanName.id = 'alertcitydv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_ciudad').parent('.col').append(spanName);                                  
    }
}

/**
* Funcion para mostrar el mensaje de validación del campo tema
* 
* @date 11/12/2021
* @author Carolina Arias
* @param boolean: booleanValidTypeClient
* @return NULL
*/ 
function alertTypeClient(booleanValidTypeClient){
    if(!booleanValidTypeClient){ 
        if($('#alertyosoy').length){
            $('#alertyosoy').remove();
        }   
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode("Seleccione una opcion.");
        spanName.style.cssText = 'color:red';
        spanName.id = 'alertyosoy';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_tema').parent('.col').append(spanName);    
    }
}

/**
* Funcion para mostrar el mensaje de validación del campo tema
* 
* @date 11/12/2021
* @author Carolina Arias
* @param boolean: booleanValidCase
* @param boolean: booleanEmpty
* @return NULL
*/ 
function alertCase(booleanValidCase, booleanEmpty){
    if(!booleanValidCase){ 
        if($('#alerttemadv').length){
            $('#alerttemadv').remove();
        }
        let stringMessage = booleanEmpty ? "El campo Asunto no puede estar vacío." : "Has ingresado un asunto invalido, recuerda que este campo debe contener solo texto.";
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode(stringMessage);
        spanName.style.cssText = 'color:red';
        spanName.id = 'alerttemadv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_tema').parent('.col').append(spanName);
    }
}

export function showPolicies() {
    window._genesys.widgets.bus.command('Toaster.open', {
        type: 'generic',
        title: 'Tratamiento de datos personales',
        body: '<div id="mtexto"><b><center>Autorización de datos personales:</b><br>La Caja de Compensación Familiar COMFENALCO ANTIOQUIA, en cumplimiento de lo establecido en los articulos 14 y 15 del Decreto 1377 de 2013, reglamentario de la Ley Estatutaria 1581 de 2012, cuenta con una Política de Tratamiento de datos personales, publicada <a href="https://www.comfenalcoantioquia.com/Default.aspx?tabid=238&id=2863" target="_blank">aquí.</a></center></div>',
        icon: 'chat',
        controls: 'close',
        immutable: false,
    });
}

/**
* Funcion para validar que un dato sea de tipo numerico
* 
* @date 11/12/2021
* @author Carolina Arias
* @param int: dato
* @return boolean: allValid
*/ 
function validateTypeNumber(dato) {
    var checkOK = "0123456789" + "0123456789";
    var checkStr = dato;
    var allValid = true;

    for (let i = 0; i < checkStr.length; i++) {
        let ch = checkStr.charAt(i);
        let j = 0;
        for (j; j < checkOK.length; j++) {
            if (ch == checkOK.charAt(j))
                break;
        }
        if (j == checkOK.length) {
            allValid = false;
            break;
        }
    }

    return allValid;
}

/**
* Funcion para validar que un dato sea de tipo string
* 
* @date 11/12/2021
* @author Carolina Arias
* @param string: dato
* @return boolean: textValid
*/ 
function validateTypeText(dato) {
    var filter6 = /^[A-Za-z áéíóúñüàè\_\-\.\s\xF1\xD1]+$/;
    var textValid = false;
    if (filter6.test(dato)) {
        textValid = true;
    } 

    return textValid;
}

/**
* Funcion para validar que un dato sea de tipo string
* 
* @date 11/12/2021
* @author Carolina Arias
* @param string: email
* @r
*/
function validateTypeEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function prefix(intNum) {
    return intNum < 10 ? ("0" + intNum) : intNum; 
}

/**
* Funcion para calcular la dieferencia entre horas
* 
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param int: intNum
* @return string: intNum
*/ 
function calculateDifference(dateInitDate, dateEndDate){
    var conversionTable = {
        intSeconds: 1000,
        intMinutes: 60*1000,
        intHours: 60*60*1000
    };

    var convertTime = (opts) => 
        Object.keys(opts).reduce((end, timeKey) => (
            end + opts[timeKey] * conversionTable[timeKey]
        ), 0)

    var dateInit = new Date("01/01/2000 "+dateInitDate);
    var dateEnd = new Date("01/01/2000 "+dateEndDate);

    var intDiff = dateEnd.getTime() - dateInit.getTime();

    var intMsec = intDiff;
    var intHours = Math.floor(intMsec / 1000 / 60 / 60);
    intMsec -= intHours * 1000 * 60 * 60;
    var intMinutes = Math.floor(intMsec / 1000 / 60);
    intMsec -= intMinutes * 1000 * 60;
    var intSeconds = Math.floor(intMsec / 1000);

    return convertTime({
        intHours: prefix(intHours),
        intMinutes: prefix(intMinutes),
        intSeconds: prefix(intSeconds)
    });
}

/**
* Funcion para validar si se encuentra en la ventana del chat
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
*/
export function validatePopUp()
{
	if(config.popUp != null){
        config.popUp.close();
    }

	config.popUp = window.open("popup.html", "Comfenalco" ,"height=210px, width=319px");

	if (config.popUp == null || typeof(config.popUp)=='undefined') {
		playtoNav();
		config.windowFocus = 0;
	} else {
		config.popUp.focus();
	}
}