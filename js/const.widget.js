export const config = {
    // urlDomain: "https://asistenciawebv2.grupokonecta.co:8443/EbChatComfenalco/",
    urlDomain: "",
    // urlSkill: "https://widget.grupokonecta.co/genesys/2/chat/comfenalco",
    urlSkill: "https://widget.grupokonecta.co/genesys/2/chat/allus",
    urlLimeSurvey: "https://limesurvey2.grupokonecta.co:5004/lime_calidad/allusIntegracionChat.php?id_encuesta=46817",
    urlTimeServer: 'https://asistenciawebv2.grupokonecta.co:8443/api_horario/hora',
    intervalTimeC: null,
    booleanWait: false,
    boooleanAgentConected: 0,
    SessionID: "",
    stringNameAgent: "",
    stringNamUser: "",
    popUp: null,
    windowFocus: true,
    typeChat: "EbChatComfenalco",
    timeControlWeek: ["08:00", "19:00", "19:00:00"],
    timeControlWeekend: ["08:00", "12:00", "12:00:00"],
    HTMLtimeControl: `
        <div class='form form-controlhorario' role='form' style='display: block;'>
            <div class="row">
                <div class="col-12 title">
                    En este momento el servicio de Asesor Virtual Comfenalco no se encuentra disponible.
                    <br>Nuestro horario de atención es:
                </div>
            </div>
            <div class="row">
                <div class="col-12 controlhorario">
                    <center>
                    Lunes a viernes 8:00 a.m. a 7:00 p.m. Sábados 8:00 a.m. a 12:00 p.m, domingos y festivos no prestamos servicio.
                    <br>¡Gracias por ponerte en contacto con nosotros!
                    <br>Call center: 444 71 10
                    </center>
                    </div>
                </div>
            </div>
        </div>`
};